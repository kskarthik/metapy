from docx import Document
import docx.opc.exceptions as docex
from PyPDF2 import PdfFileReader
import PyPDF2.utils as pdfex
from PIL import Image, ExifTags

# Extract Metadata from various file formats i.e. docx, pdf, jpeg


class Metadata:

    #  Returns dictionary of docx metadata, Nil value is None.
    def docx(filePath):

        try:
            with open(filePath, 'rb') as f:

                doc = Document(f).core_properties

                return {
                    'author': doc.author or 'None',
                    'category': doc.category or 'None',
                    'comments': doc.comments or 'None',
                    'content_status': doc.content_status or 'None',
                    'created': str(doc.created),
                    'identifier': doc.identifier or 'None',
                    'keywords': doc.keywords or 'None',
                    'language': doc.language or 'None',
                    'last_modified_by': doc.last_modified_by or 'None',
                    'last_printed': str(doc.last_printed),
                    'modified': str(doc.modified),
                    'revision': doc.revision,
                    'subject': doc.subject or 'None',
                    'title': doc.title or 'None',
                    'version': doc.version or 'None'
                }
        except docex.PackageNotFoundError:
            return 'DocxErr'

        except docex.OpcError:
            return 'DocxErr'

        except ValueError:
            return 'DocxErr'

    # Returns dictionary of pdf metadata
    def pdf(filePath):

        try:
            with open(filePath, 'rb') as f:
                return PdfFileReader(f).getDocumentInfo()

        except pdfex.PdfReadError:
            return 'PdfErr'

        except pdfex.PdfStreamError:
            return 'PdfErr'

        except UnicodeEncodeError:
            return 'PdfErr'

        except ValueError:
            return 'PdfErr'

        except TypeError:
            return 'PdfErr'

    # Returns dict of image metadata. Returns 'None' if metadata doen't exist
    def image(filePath):

        with open(filePath, 'rb') as path:

            info = Image.open(path).getexif()

            if info != {}:

                meta = {}

                for k in info:
                    meta.update({ExifTags.TAGS[k]: info[k]})

                return meta
            else:
                return 'None'


if __name__ == "__main__":

    print('\n', Metadata.pdf('demo.pdf'), '\n')
    print(Metadata.docx('demo.docx'), '\n')
    print(Metadata.image('Canon_40D.jpg'), '\n')
