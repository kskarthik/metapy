# from elasticsearch import Elasticsearch
from metapy import Metadata
import os

# server = Elasticsearch('http://139.59.71.111:9200/')


# class Data:
#     '''
#     Upload metadata to server
#     '''
#     def upload(obj):
#         print('uploading...')

#     def info():
#         print(server.get(index='docx', id=1)['_source'])


path = '/home/sk/Downloads/026'

for fileName in os.listdir(path):

    is_file = os.path.isfile(f'{path}/{fileName}')

    if is_file and fileName.lower().endswith('.docx'):
        obj = Metadata.docx(f'{path}/{fileName}')

        if obj != 'DocxErr':
            print('DOCX', obj, '\n')

    elif is_file and fileName.lower().endswith('.jpg'):
        obj = Metadata.image(f'{path}/{fileName}')

        if obj != 'None':
            print('IMAGE', obj, '\n')

    elif is_file and fileName.lower().endswith('.pdf'):
        obj = Metadata.pdf(f'{path}/{fileName}')

        if obj != 'PdfErr' and obj != 'None':
            print('PDF', obj, '\n')
