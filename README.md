# Metapy
A python library which extracts metadata from images, docx, pdf's

## Installation

- Clone this repository
- cd into the folder
- Install pip dependencies `pip3 install -r requirements.txt`

## Usage

```
# import library
from metapy.metapy import Metadata


Metadata.docx('demo.docx') # for microsoft word docs 

Metadata.pdf('demo.pdf') # for pdf files

Metadata.image('Canon_40D.jpg') # for images
```